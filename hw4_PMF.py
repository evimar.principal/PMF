# -*- coding: utf-8 -*-
"""
Created on Tue Mar 28 10:54:04 2017

@author: evimar
"""
import sys
from scipy import sparse
import numpy as np

datafile = sys.argv[1]

N_iter = 50

#leemos el archivo del parametro
data = np.genfromtxt(datafile, delimiter=',')

Nusers, Nobjects, _ = map(int, data.max(axis=0))
#Creamos una matriz dispersa con los ratings de los usuarios
#acerca de los objetos
matrix = sparse.lil_matrix((Nusers, Nobjects))

#n1 Nusers
#n2 Nobjects

for rate in data:
    user, objeto, rating = map(int, rate)
    matrix[user-1,objeto-1] = rating
#Los indices en el archivo comienzan con uno
#en la matriz los indices comienzan en cero

#PMF Probabilistic Matrix Factorization
#Cordinate Ascent

matrixC = matrix.todense()

d = 5
lamb = 2.0
sigma2 = 0.1
ites = [9,24,49]
#Ramdom init for v
mean = np.zeros(d)
cov = np.eye(d)
V = np.random.multivariate_normal(mean, cov, Nobjects)
constante = lamb*sigma2*np.eye(d)

Ls = []

#Nro de iteraciones
for iteracion in range(N_iter):
    #update U
    inverse = np.linalg.inv(constante + V.transpose().dot(V))
    MV = matrixC.dot(V)
    U = MV.dot(inverse)
    
    #update V   
    inverse = np.linalg.inv(constante + U.transpose().dot(U))
    MU = matrixC.transpose().dot(U)
    V = MU.dot(inverse)
    
    #calculate the objective function
    
    UTV = matrixC.transpose() - V.dot(U.transpose())
    L = np.multiply(UTV, UTV).sum() / -2.0 / sigma2
    L = L - lamb/2. * (np.multiply(U,U).sum() + np.multiply(V,V).sum())
    Ls.append(L)    
    
    if iteracion in ites:
        str_iter = str(iteracion+1)
        filenameU, filenameV = "U-"+str_iter+".csv", "V-"+str_iter+".csv"
        np.savetxt(filenameU, U, delimiter=',')
        np.savetxt(filenameV, V, delimiter=',')
        
    
filenameO = "objective.csv"
np.savetxt(filenameO, Ls)    
    
